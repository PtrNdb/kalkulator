def sum_floats():
    print("Wybrano operacje dodawania")
    a = float(input("Podaj pierwsza liczbe:\n"))
    b = float(input("Podaj druga liczbe:\n"))
    result = round((a + b), 2)
    print("Wynik operacji dodawania to: " + str(result))
def subs_floats():
    print("Wybrano operacje odejmowania")
    a = float(input("Podaj pierwsza liczbe:\n"))
    b = float(input("Podaj druga liczbe:\n"))
    result = round((a - b), 2)
    print("Wynik operacji odejmowania to: " + str(result))
def multiply_floats():
    print("Wybrano operacje mnozenia")
    a = float(input("Podaj pierwsza liczbe:\n"))
    b = float(input("Podaj druga liczbe:\n"))
    result = round((a * b), 2)
    print("Wynik operacji mnozenia to: " + str(result))
def divide_floats():
    print("Wybrano operacje dzielenia")
    a = float(input("Podaj pierwsza liczbe:\n"))
    b = float(input("Podaj druga liczbe:\n"))
    result = round((a / b), 2)
    print("Wynik operacji dzielenia to: " + str(result))
def menu():
    print("""
===============Kalkulator=================
1. Dodawanie
2. Odejmowanie
3. Mnozenie
4. Dzielenie
==========================================
    """)
    choise = int(input())
    if (choise == 1):
        sum_floats()
    if (choise == 2):
        subs_floats()
    if (choise == 3):
        multiply_floats()
    if (choise == 4):
        divide_floats()
menu()

